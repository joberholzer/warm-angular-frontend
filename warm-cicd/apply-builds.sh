mapfile -t buildArray < ./builds/builds.txt

applyBuild(){
oc apply -f $1 -n warm-app-build
}

oc project warm-app-build
for ii in "${buildArray[@]}"; do
applyBuild $ii
done