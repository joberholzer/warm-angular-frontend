mapfile -t projectsArray < projects/projects.txt

setupSecrets(){
oc create -f $1 -n $2
}

for i in "${projectsArray[@]}"; do
oc delete project $i
done