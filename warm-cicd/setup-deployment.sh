DEV=warm-app-dev
STAGING=warm-app-staging
UAT=warm-app-uat
PROD=warm-app-prod
CICD1=warm-app-build

#Setup Deployment
echo "Setting up deployment"
mapfile -t deployArray < ./projects/deployment-namespaces.txt
mapfile -t servArray < ./projects/build-serviceaccounts.txt

setAppTemplate(){
oc process -f $1 -p APPLICATION_NAME=$2 -p NAMESPACE=$3 -p DNS_PREFIX=$4 -p ENV=$5 -p SA_NAMESPACE=$6 -p HOST_PORT=$7 | oc apply -f-      
}

<<COMMENT
DEPLOY_APP="./deployment/template-mod.yml"
oc project warm-app-dev
setAppTemplate $DEPLOY_APP warm-angular warm-app-dev warm-angular dev $CICD1 8080
setAppTemplate $DEPLOY_APP warm-business-service warm-app-dev warm-business-service dev $CICD1 8081
setAppTemplate $DEPLOY_APP warm-whatsapp-core warm-app-dev warm-whatsapp-core dev $CICD1 8081
COMMENT

setRoles(){
oc policy add-role-to-user edit $1 -n $2
}

for ii in "${servArray[@]}"; do
setRoles $ii $DEV
setRoles $ii $STAGING
setRoles $ii $UAT
setRoles $ii $PROD
done