#!/bin/bash
echo "Setting up projects"
DN=""
setDisplayName(){
    case $1 in
    ("warm-app-build") DN="WARM BUILD" ;;
    ("warm-app-dev") DN="WARM DEV" ;;
    ("warm-app-staging") DN="WARM STAGING" ;;
    ("warm-app-uat") DN="WARM UAT" ;;
    ("warm-app-prod") DN="WARM PROD" ;;
    esac
}

mapfile -t projects < ./projects/projects.txt
for i in "${projects[@]}"; do
setDisplayName $i
echo $DN
oc new-project $i --display-name="$DN"
done

#Setup config
echo "Setting up config files"
mapfile -t buildArray < ./projects/build-namespaces.txt
mapfile -t configArray < ./config/configs.txt

CM="./config/config-map.yaml"
MS="./config/settings.xml"
MS_LABEL="mavensettings"
CICD1=warm-app-build

for i in "${buildArray[@]}"; do
oc create -f $CM -n $i
oc create configmap $MS_LABEL --from-file $MS -n $i
done

for i in "${configArray[@]}"; do
echo "Setup configs"
oc create -f $i -n $CICD1
done

#Setup Deployment
echo "Setting up deployment"
mapfile -t deployArray < ./projects/deployment-namespaces.txt

setAppTemplate(){
oc process -f $1 -p APPLICATION_NAME=$2 -p NAMESPACE=$3 -p DNS_PREFIX=$4 -p ENV=$5 -p SA_NAMESPACE=$6 -p HOST_PORT=$7 | oc apply -f-      
}

DEPLOY_APP="./deployment/template-mod.yml"

for i in "${deployArray[@]}"; do
if [ $i == "warm-app-dev" ]; then
    echo "setting up develop deployments"
    setAppTemplate $DEPLOY_APP warm-angular $i warm-angular dev $CICD1 8080
    setAppTemplate $DEPLOY_APP warm-business-service $i warm-business-service dev $CICD1 8081
    setAppTemplate $DEPLOY_APP warm-whatsapp-core $i warm-whatsapp-core dev $CICD1 8081
elif [ $i == "warm-app-staging" ]; then
    echo "setting up qa deployments"
    setAppTemplate $DEPLOY_APP warm-angular $i warm-angular qa $CICD1 8080
    setAppTemplate $DEPLOY_APP warm-business-service $i warm-business-service qa $CICD1 8081
    setAppTemplate $DEPLOY_APP warm-whatsapp-core $i warm-whatsapp-core qa $CICD1 8081
elif [ $i == "warm-app-uat" ]; then
    echo "setting up pre-prod deployments"
    setAppTemplate $DEPLOY_APP warm-angular $i warm-angular uat $CICD1 8080   
    setAppTemplate $DEPLOY_APP warm-business-service $i warm-business-service uat $CICD1 8081
    setAppTemplate $DEPLOY_APP warm-whatsapp-core $i warm-whatsapp-core uat $CICD1 8081
elif [ $i == "warm-app-prod" ]; then
    echo "setting up release deployments"
    setAppTemplate $DEPLOY_APP warm-angular $i warm-angular prod $CICD1 8080
    setAppTemplate $DEPLOY_APP warm-business-service $i warm-business-service prod $CICD1 8081
    setAppTemplate $DEPLOY_APP warm-whatsapp-core $i warm-whatsapp-core prod $CICD1 8081
fi
done

DEV=warm-app-dev
STAGING=warm-app-staging
UAT=warm-app-uat
PROD=warm-app-prod

mapfile -t servArray < ./projects/build-serviceaccounts.txt

setJenkins(){
oc new-app jenkins-persistent -p ENABLE_OAUTH=true -p MEMORY_LIMIT=4Gi -p VOLUME_CAPACITY=10Gi -n $1
}

for i in "${buildArray[@]}"; do
setJenkins $i
done

setRoles(){
oc policy add-role-to-user edit $1 -n $2
}

for ii in "${servArray[@]}"; do
setRoles $ii $DEV
setRoles $ii $STAGING
setRoles $ii $UAT
setRoles $ii $PROD
done

#Setup builds & pipelines
mapfile -t buildArray < ./builds/builds.txt

setBuild(){
oc create -f $1 -n warm-app-build
}

oc project warm-app-build
for ii in "${buildArray[@]}"; do
setBuild $ii
done

#Setup RBAC
BASIC=basic-user
ADMIN=admin
EDIT=edit
VIEW=view

mapfile -t adminArray < ./rbac/rbac-admin.txt

rbac(){
    oc adm policy add-role-to-user $1 $2 -n "$3"
}

for i in "${adminArray[@]}"; do
rbac $ADMIN $i $CICD1
rbac $ADMIN $i $DEV
rbac $ADMIN $i $STAGING
rbac $ADMIN $i $UAT
rbac $ADMIN $i $PROD
done

#Setup secrets
mapfile -t secretsArray < ./secrets/project-secrets.txt

setupSecrets(){
oc create -f $1 -n $2
}

for i in "${secretsArray[@]}"; do
setupSecrets $i $CICD1
setupSecrets $i $CICD1
setupSecrets $i $CICD1
done

for i in "${buildArray[@]}"; do
oc create secret generic bitbucket --from-file=ssh-privatekey=$HOME/.ssh/id_rsa -n $i
done