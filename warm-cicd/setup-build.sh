mapfile -t buildArray < ./builds/builds.txt

setBuild(){
oc create -f $1 -n warm-app-build
}

oc project warm-app-build
for ii in "${buildArray[@]}"; do
setBuild $ii
done