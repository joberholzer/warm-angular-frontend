import { environment } from 'src/environments/environment';


export const OAuthSettings = {
    appId: '753a7a2b-1e41-4047-ab34-796105fcb703',
    authority: 'https://login.microsoftonline.com/932fad99-d6a1-437f-a78c-b0be562227ff/',
    validateAuthority: true,
    redirectUri: environment.redirectURl,
    navigateToLoginRequestUrl: true,
    scopes: [
        "user.read"
      ]
  };
