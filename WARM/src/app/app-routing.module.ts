import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

//import { LoginComponent } from "./components/login/login.component";
import { HomescreenComponent } from './containers/homescreen/homescreen.component';

import { MsalGuard } from '@azure/msal-angular';
import { UserSelectComponent } from './containers/user-select/user-select.component';
import { UnauthorizedComponent } from './containers/unauthorized/unauthorized.component';

const routes: Routes = [
  { path: '', canActivate:[MsalGuard], children: [
    { path: "", pathMatch: "full", redirectTo: "home"},
    { path: "home", component: HomescreenComponent },
    { path: "unauthorized", component: UnauthorizedComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
