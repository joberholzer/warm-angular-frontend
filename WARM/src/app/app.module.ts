import { BrowserModule } from "@angular/platform-browser";
import { NgModule, ErrorHandler } from "@angular/core";
import { AngularMaterialModule } from "./modules/angular-material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ServiceWorkerModule } from "@angular/service-worker";
import { MsalModule } from '@azure/msal-angular';
import { OAuthSettings } from '../oauth';

import { AppComponent } from "./app.component";
//import { LoginComponent } from "./components/login/login.component";
import { HomescreenComponent } from './containers/homescreen/homescreen.component';

import { environment } from "../environments/environment";

//import { ErrorInterceptor } from "./helpers/error.interceptor";
//import { JwtInterceptor } from "./helpers/jwt.interceptor";

//import { AuthService } from "../app/services/authorization/auth.service";
//import { GlobalErrorHandlerService } from "../app/services/errorhandling/global-error-handler.service";
import { HomescreenModule } from './modules/homescreen.module';
import { UserSelectComponent } from './containers/user-select/user-select.component';
import { UnauthorizedComponent } from './containers/unauthorized/unauthorized.component';

@NgModule({
  declarations: [
    AppComponent,
  //  LoginComponent,
    HomescreenComponent,
  UserSelectComponent,
  UnauthorizedComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    HomescreenModule,
    MsalModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production
    }),
    MsalModule.forRoot({
      clientID: OAuthSettings.appId,
      authority: OAuthSettings.authority,
      validateAuthority: OAuthSettings.validateAuthority,
      redirectUri: OAuthSettings.redirectUri,
      postLogoutRedirectUri: OAuthSettings.redirectUri,
      // navigateToLoginRequestUrl: OAuthSettings.navigateToLoginRequestUrl
    })
  ],
  exports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HomescreenModule
  ],
  providers: [
    //{ provide: ErrorHandler, useClass: GlobalErrorHandlerService },
   // { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
   // { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
   // AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
