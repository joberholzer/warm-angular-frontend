import { SidebarComponent } from '../components/sidebar/sidebar.component';
import { ChatWindowComponent } from '../components/chat-window/chat-window.component';
import { ConversationsBarComponent } from '../components/conversations-bar/conversations-bar.component';
import { ClientWindowComponent } from '../components/client-window/client-window.component';
import { TeamLeadWindowComponent } from '../components/teamlead-window/teamlead-window.component';
import { RelationshipManagerWindowComponent } from '../components/relationship-manager-window/relationship-manager-window.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from './angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TransactionalBankerProfileComponent } from '../components/transactional-banker-profile/transactional-banker-profile.component';
import { ModalComponent } from '../components/modal/modal.component';
import { LandingPageComponent } from '../components/landing-page/landing-page.component';
import { LoadingComponent } from '../components/loading/loading.component';

@NgModule({
    imports: [CommonModule, ReactiveFormsModule, AngularMaterialModule, FlexLayoutModule],
    exports: [
        SidebarComponent,
        ConversationsBarComponent,
        ChatWindowComponent,
        ClientWindowComponent,
        RelationshipManagerWindowComponent,
        TeamLeadWindowComponent,
        TransactionalBankerProfileComponent,
        ModalComponent,
        LandingPageComponent,
        LoadingComponent
    ],
    declarations: [
        SidebarComponent,
        ConversationsBarComponent,
        ChatWindowComponent,
        ClientWindowComponent,
        RelationshipManagerWindowComponent,
        TeamLeadWindowComponent,
        TransactionalBankerProfileComponent,
        ModalComponent,
        LandingPageComponent,
        LoadingComponent
    ],
    providers: []
 })
 export class HomescreenModule {
 }
