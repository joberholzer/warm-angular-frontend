import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Customer } from 'src/app/models/customer';
import { Conversation } from 'src/app/models/conversation';
import { ConversationService } from 'src/app/services/conversation/conversation.service';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { Observable } from 'rxjs';
import { Message } from 'src/app/models/message';
import { formatDate } from '@angular/common';
import { Portfolio } from 'src/app/models/portfolio';
import { isNullOrUndefined } from 'util';
@Component({
  selector: 'app-client-window',
  templateUrl: './client-window.component.html',
  styleUrls: ['./client-window.component.scss']
})
export class ClientWindowComponent implements OnInit {
  @Input() currentClientId: string;
  @Input() currentConversation: Conversation;

  @Output() showConversationScreen = new EventEmitter();
  @Output() returnToConversation = new EventEmitter();

  client: Customer = new Customer();

  clientConversations: Array<Conversation>;
  allClientConversations: Array<Conversation>;

  showFullHistory = false;
  currentDate = new Date();
  currentYear = this.currentDate.getFullYear();
  currentMonth =   formatDate(this.currentDate, 'MMM', 'en-ZA');

  monthsList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  yearsList = [this.currentYear, this.currentYear - 1, this.currentYear - 2, this.currentYear - 3, this.currentYear - 4];

  openMonthFilter = false;
  openYearFilter = false;
  maxConvos= 5;
  
  constructor(private conversationService: ConversationService, private customerService: CustomerService) { }

  ngOnInit(): void {
      this.customerService
      .getCustomerById(this.currentClientId)
      .subscribe((data: Customer) => {
        this.client = data;
      });
      
      this.conversationService
      .getConversationsByCustomerId(this.currentClientId)
      .subscribe((data: Conversation[]) => {
        this.clientConversations = data;
        this.allClientConversations = data;
      });
  }

  goToConversation(conversation){
    this.showConversationScreen.emit(conversation);
  }

  getConversationLastMessage(conversation): Message {
    return conversation.messages[conversation.messages.length - 1];
  }

  getShortTime(messageDateTime: string){
    const formatedTime =  formatDate(messageDateTime, 'd MMM y, h:mm:ss a', 'en-ZA');
    return formatedTime;
  }

  getRMName(clientPortfolio: Portfolio){
    if(!isNullOrUndefined(clientPortfolio)){
      return `${clientPortfolio.name} ${clientPortfolio.surname}`;
    }
    else{
      return '';
    }
  }

  getClientName(clientObj: Customer){
    if(!isNullOrUndefined(clientObj.firstName)){
      return clientObj.firstName + ' ' + clientObj.lastName;
    }
    else{
      return clientObj.mobileNumber;
    }
  }

  conversationHasMessages(conversation){
    return !isNullOrUndefined(conversation.messages) && conversation.messages.length > 0;
  }

  returnConversation(){
    this.returnToConversation.emit(this.currentConversation);
  }

  toggleFilter(filterType: string){
    if(filterType.toLowerCase() === 'year'){
      this.openYearFilter = !this.openYearFilter;
    }
    else if(filterType.toLowerCase() === 'month'){
      this.openMonthFilter = !this.openMonthFilter;
    }
  }

  setCurrent(filterType: string, newValue){
    if(filterType.toLowerCase() === 'year'){
      this.currentYear = newValue;
    }
    else if(filterType.toLowerCase() === 'month'){
      this.currentMonth = newValue;
    }
    this.filterByDate();
  }

  filterByDate(){

    this.clientConversations =  this.allClientConversations.filter(this.compareDates);
  }

  compareDates = (conversation: Conversation) => {
    if(this.conversationHasMessages(conversation)){
      const preferredDate = new Date(this.currentYear + ' ' + this.currentMonth);
      const convoDate = new Date(conversation.messages[0].messageTimestamp);

      if(preferredDate.getFullYear() === convoDate.getFullYear()){
        if(preferredDate.getMonth() === convoDate.getMonth()){
          return true;
        }
      }
      else{
        return false;
      }
    }
    else{
      return false;
    }
  }

  hasNoConversations(bankerConvos: Conversation[]){
    if(bankerConvos !== null && bankerConvos !== undefined){
      return bankerConvos.length === 0;
    }
  }

  showButton(conversations: Conversation[]){
    if(conversations !== null && conversations !== undefined){
      const convosWithMsg = conversations.filter(this.conversationHasMessages);
      if(!this.showFullHistory && (convosWithMsg.length > 5)){
        return true;
      }
      this.maxConvos = convosWithMsg.length - 1;
    }
    return false;
  }
}
