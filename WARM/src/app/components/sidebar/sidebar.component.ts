
import { Component, OnInit, Input, EventEmitter, Output, SimpleChanges } from "@angular/core";
import { AuthService } from 'src/app/services/authService/auth.service';
import { isNullOrUndefined } from 'util';


@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"]
})
export class SidebarComponent implements OnInit {
  @Input() menuItems: any[];
  @Output() selectedTabChange = new EventEmitter();
  @Input() totalConversations: number;

  statusChat: boolean = true;
  statusPort: boolean = false;
  statusLogout: boolean = false;
  currentTab : string;
  notificationAvailable: boolean = true;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(!isNullOrUndefined(changes.menuItems)){
      this.currentTab = changes.menuItems.currentValue[0].label.toLowerCase();
      }
  }

  async signIn(): Promise<void> {
    await this.authService.signIn();
  }

  async signOut(): Promise<void> {
    await this.authService.signOut();
  }

  tabChanged(currentTab: string) {
    this.currentTab = currentTab.toLowerCase();
    this.selectedTabChange.emit(this.currentTab);
    if (this.currentTab === "chat") {
      this.statusChat = true;
      this.statusPort = false;
      this.statusLogout = false;
    }
    if (this.currentTab === "portfolios") {
      this.statusPort = true;
      this.statusChat = false;
      this.statusLogout = false;
    }
    if (this.currentTab === "logout") {
      this.statusLogout = true;
      this.statusChat = false;
      this.statusPort = false;
    }
  }

  isCurrentTab(tabSelected: string){
    return tabSelected.toLowerCase() === this.currentTab.toLowerCase();
  }

  isMessagingTab(tabLabel: string){
    if(tabLabel.toLowerCase() === 'escalations' || tabLabel.toLowerCase() === 'chat'){
      return true;
    }
    else{
      return false;
    }
  }
}
