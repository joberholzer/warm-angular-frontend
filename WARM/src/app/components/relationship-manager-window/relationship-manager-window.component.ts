import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from "@angular/core";
import { CustomerService } from 'src/app/services/customer/customer.service';
import { Portfolio } from 'src/app/models/portfolio';
import { Customer } from 'src/app/models/customer';
import { isNullOrUndefined } from 'util';

@Component({
  selector: "app-relationship-manager-window",
  templateUrl: "./relationship-manager-window.component.html",
  styleUrls: ["./relationship-manager-window.component.scss"]
})
export class RelationshipManagerWindowComponent implements OnInit {
  @Input() currentPortfolio: Portfolio = new Portfolio();
  @Output() showClientProfile = new EventEmitter();

  bankerCustomers: Array<Customer>;
  //displayLanding: boolean = true;
  username: string = "RM name here";

  constructor(private customerService: CustomerService) {}

  ngOnInit(): void {
    this.customerService
    .getCustomersByPortfolioId(this.currentPortfolio.portfolioId)
    .subscribe((data: Customer[]) => {
      this.bankerCustomers = data;
    });
  }

  ngOnChanges(changes: SimpleChanges) {

    if (!isNullOrUndefined(changes.currentPortfolio.currentValue)){
      this.customerService
      .getCustomersByPortfolioId(changes.currentPortfolio.currentValue.portfolioId)
      .subscribe((data: Customer[]) => {
        this.bankerCustomers = data;
      });
    }
  }
  
  displayUserProfile(currentCustomerId) {
    this.showClientProfile.emit(currentCustomerId);
  }

  getClientName(clientObj: Customer){
    return clientObj.firstName === null ? clientObj.mobileNumber : `${clientObj.firstName} ${clientObj.lastName}`;
  }

  filteredCustomers : Array<Customer>;
  search = "";
  showSearchResults = false;

  onSearch(event: any) {
    this.search = event.target.value;
    this.filteredCustomers = this.bankerCustomers.filter((item: Customer) => {
        return (item.firstName === this.search || item.lastName === this.search || this.getClientName(item) === this.search || this.search === item.customerSAId || this.search === item.mobileNumber);
    });
    if (this.filteredCustomers.length === 0 && this.search !== undefined  && this.search !== "") {
      let regex = new RegExp(/^[0-9]*$/);
      let isNum: boolean = regex.test(this.search);
      if (isNum) {
        this.numberSearch();
      } else {
        this.nameSearch();
      }
    }
    if (this.search === "") {
      this.showSearchResults = false;
    } else this.showSearchResults = true;
  }

  numberSearch = () => {
    //search for ID number and Cell number
    this.bankerCustomers.forEach((element) => {
      let idNum = element.customerSAId;
      let cellNum = element.mobileNumber;
      const search = this.search.toLowerCase();
      let searchResult = "";
      let searchResultCell = "";

      const shortestIdSearch = search.length < idNum.length ? search.length : idNum.length;
      const shortestCellSearch = search.length < cellNum.length ? search.length : cellNum.length;
      for (let i = 0; i < shortestIdSearch; i++) {
        const character = idNum[i].toLowerCase();
        if (character === search[i]) {
          searchResult += search[i];
          const lengthOfMatchedSearch = searchResult.length;
          const searchElement = idNum.slice(0, lengthOfMatchedSearch);
          if (lengthOfMatchedSearch === search.length) {
            if (searchResult === searchElement) {
              this.filteredCustomers.push(element);
            }
          }
        }
      }
      for (let index = 0; index < shortestCellSearch; index++) {
        const character = cellNum[index].toLowerCase();

        if (character === search[index]) {
          searchResultCell += search[index];
          const lengthOfMatchedSearch = searchResultCell.length;
          const searchElement = cellNum.slice(0, lengthOfMatchedSearch);
          if (lengthOfMatchedSearch === search.length) {

            if (searchResultCell === searchElement) {
              this.filteredCustomers.push(element);
            }
          }
        }
      }
    });
  };

  nameSearch = () => {
    //search for ID number and Cell number
    this.bankerCustomers.forEach((element) => {
      let idNum = this.getClientName(element).toLowerCase();
      let cellNum = element.lastName.toLowerCase();
      let emailAddress = element.customerEmail;
      const search = this.search.toLowerCase();
      let searchResult = "";
      let searchResultCell = "";
      let searchResultEmail = "";

      const shortestIdSearch = search.length < idNum.length ? search.length : idNum.length;
      const shortestCellSearch = search.length < cellNum.length ? search.length : cellNum.length;
      for (let i = 0; i < shortestIdSearch; i++) {
        const character = idNum[i].toLowerCase();
        if (character === search[i]) {
          searchResult += search[i];
          const lengthOfMatchedSearch = searchResult.length;
          const searchElement = idNum.slice(0, lengthOfMatchedSearch);
          if (lengthOfMatchedSearch === search.length) {
            if (searchResult === searchElement) {
              this.filteredCustomers.push(element);
            }
          }
        }
      }
      for (let index = 0; index < shortestCellSearch; index++) {
        const character = cellNum[index].toLowerCase();

        if (character === search[index]) {
          searchResultCell += search[index];
          const lengthOfMatchedSearch = searchResultCell.length;
          const searchElement = cellNum.slice(0, lengthOfMatchedSearch);
          if (lengthOfMatchedSearch === search.length) {

            if (searchResultCell === searchElement) {
              this.filteredCustomers.push(element);
            }
          }
        }
      }

      if(emailAddress !== null){
        for (let index = 0; index < shortestCellSearch; index++) {
          const character = emailAddress[index].toLowerCase();
  
          if (character === search[index]) {
            searchResultEmail += search[index];
            const lengthOfMatchedSearch = searchResultEmail.length;
            const searchElement = emailAddress.slice(0, lengthOfMatchedSearch);
            if (lengthOfMatchedSearch === search.length) {
  
              if (searchResultEmail === searchElement) {
                this.filteredCustomers.push(element);
              }
            }
          }
        }
      }
    });
  };

  getRMName(currentPortfolio: Portfolio){
    if(!isNullOrUndefined(currentPortfolio.rmInternalUser))
      return `${currentPortfolio.rmInternalUser.name} ${currentPortfolio.rmInternalUser.surname}`;
    else
      return `${currentPortfolio.name} ${currentPortfolio.surname}`;
  }

  hasResults(){
    return this.filteredCustomers.length > 0;
  }

  hasCustomers(){
    return !isNullOrUndefined(this.bankerCustomers) && this.bankerCustomers.length > 0;
  }
}
