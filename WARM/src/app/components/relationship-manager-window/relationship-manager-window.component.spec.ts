import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelationshipManagerWindowComponent } from './relationship-manager-window.component';

describe('RelationshipManagerWindowComponent', () => {
  let component: RelationshipManagerWindowComponent;
  let fixture: ComponentFixture<RelationshipManagerWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelationshipManagerWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelationshipManagerWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
