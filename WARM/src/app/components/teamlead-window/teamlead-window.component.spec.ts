import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamLeadWindowComponent } from './teamlead-window.component';

describe('TeamleadWindowComponent', () => {
  let component: TeamLeadWindowComponent;
  let fixture: ComponentFixture<TeamLeadWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamLeadWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamLeadWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
