import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges } from '@angular/core';
import { InternaluserService } from 'src/app/services/internaluser/internaluser.service';
import { InternalUser } from 'src/app/models/internal-user';
import { Conversation } from 'src/app/models/conversation';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-teamlead-window',
  templateUrl: './teamlead-window.component.html',
  styleUrls: ['./teamlead-window.component.scss']
})
export class TeamLeadWindowComponent implements OnInit {

  constructor(private internaluserService: InternaluserService) { }

  @Input() isReAllocation: boolean;
  @Input() bankerTeamId: string;
  @Input() currentBanker: InternalUser;

  @Output() bankerProfileClicked = new EventEmitter();

  teamBankers: Array<InternalUser>;

  filteredBankers : Array<InternalUser>;
  search = "";
  showSearchResults = false;

  ngOnInit(): void {
    let selectBankerTeamId = this.bankerTeamId;

    if(this.currentBanker.team !== undefined){
      selectBankerTeamId = this.currentBanker.team.teamId.toString();
    }

    this.internaluserService.getTransactionalBankersPerTeam(selectBankerTeamId)
    .subscribe((data: InternalUser[]) => {
      this.teamBankers = data;
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    let selectBankerTeamId = '';

    if(changes.currentBanker.currentValue.team !== undefined){
      selectBankerTeamId = changes.currentBanker.currentValue.team.teamId;
    }
    else{
      selectBankerTeamId = changes.bankerTeamId.currentValue;
    }

    this.internaluserService.getTransactionalBankersPerTeam(selectBankerTeamId)
    .subscribe((data: InternalUser[]) => {
      this.teamBankers = data;
    });
  }
  
  registerProfileClick(bankerObj, isReAllocation) {
    this.bankerProfileClicked.emit({ bankerObj, isReAllocation});
  }

  countConversations(conversationsArray: Conversation[], conversationStatus: string){
    if(!isNullOrUndefined(conversationsArray) && conversationsArray.length > 0){
      return conversationsArray.filter(convo => convo.conversationStatus.toLowerCase() === conversationStatus).length;
    }
    else{
      return 0;
    }
  }

  getFullName(clientObj: InternalUser){
    return clientObj.name === null ? clientObj.username : `${clientObj.name} ${clientObj.surname}`;
  }

  onSearch(event: any) {
    this.search = event.target.value;
    this.filteredBankers = this.teamBankers.filter((item: InternalUser) => {
        return (item.name === this.search || item.surname === this.search || this.getFullName(item) === this.search || this.search === item.username);
    });
    if (this.filteredBankers.length === 0 && this.search !== undefined  && this.search !== "") {
      this.nameSearch();
    }
    if (this.search === "") {
      this.showSearchResults = false;
    } else this.showSearchResults = true;
  }

  nameSearch = () => {
    //search for ID number and Cell number
    this.teamBankers.forEach((element) => {
      let idNum = this.getFullName(element).toLowerCase();
      let cellNum = element.surname.toLowerCase();
      let username = element.username.toLowerCase();

      const search = this.search.toLowerCase();
      let searchResult = "";
      let searchResultCell = "";
      let searchResultUsername = "";

      const shortestIdSearch = search.length < idNum.length ? search.length : idNum.length;
      const shortestCellSearch = search.length < cellNum.length ? search.length : cellNum.length;
      const shortestUsernameSearch = search.length < cellNum.length ? search.length : cellNum.length;

      for (let i = 0; i < shortestIdSearch; i++) {
        const character = idNum[i].toLowerCase();
        if (character === search[i]) {
          searchResult += search[i];
          const lengthOfMatchedSearch = searchResult.length;
          const searchElement = idNum.slice(0, lengthOfMatchedSearch);
          if (lengthOfMatchedSearch === search.length) {
            if (searchResult === searchElement) {
              this.filteredBankers.push(element);
            }
          }
        }
      }
      for (let index = 0; index < shortestCellSearch; index++) {
        const character = cellNum[index].toLowerCase();

        if (character === search[index]) {
          searchResultCell += search[index];
          const lengthOfMatchedSearch = searchResultCell.length;
          const searchElement = cellNum.slice(0, lengthOfMatchedSearch);
          if (lengthOfMatchedSearch === search.length) {

            if (searchResultCell === searchElement) {
              this.filteredBankers.push(element);
            }
          }
        }
      }
      for (let currentIndex = 0; currentIndex < shortestUsernameSearch; currentIndex++) {
        const character = username[currentIndex].toLowerCase();

        if (character === search[currentIndex]) {
          searchResultUsername += search[currentIndex];
          const lengthOfMatchedSearch = searchResultUsername.length;
          const searchElement = username.slice(0, lengthOfMatchedSearch);
          if (lengthOfMatchedSearch === search.length) {

            if (searchResultUsername === searchElement) {
              this.filteredBankers.push(element);
            }
          }
        }
      }
    });
  }

  hasResults(){
    return this.filteredBankers.length > 0;
  }
}
