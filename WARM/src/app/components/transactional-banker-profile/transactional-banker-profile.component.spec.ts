import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionalBankerProfileComponent } from './transactional-banker-profile.component';

describe('TransactionalBankerProfileComponent', () => {
  let component: TransactionalBankerProfileComponent;
  let fixture: ComponentFixture<TransactionalBankerProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionalBankerProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionalBankerProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
