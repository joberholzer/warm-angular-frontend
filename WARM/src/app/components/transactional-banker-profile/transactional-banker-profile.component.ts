import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { InternalUser } from 'src/app/models/internal-user';
import { Conversation } from 'src/app/models/conversation';
import { isNullOrUndefined } from 'util';
import { formatDate } from '@angular/common';
import { Message } from 'src/app/models/message';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { InternaluserService } from 'src/app/services/internaluser/internaluser.service';
import { ConversationService } from 'src/app/services/conversation/conversation.service';

@Component({
  selector: 'app-transactional-banker-profile',
  templateUrl: './transactional-banker-profile.component.html',
  styleUrls: ['./transactional-banker-profile.component.scss']
})
export class TransactionalBankerProfileComponent implements OnInit {

  @Input() bankerObject: InternalUser;
  @Output() showConversationAllocateScreen = new EventEmitter();
  bankerConversations = new Array<Conversation>();
  allBankerConversations = new Array<Conversation>();

  currentDate = new Date();
  currentYear = this.currentDate.getFullYear();
  currentMonth =   formatDate(this.currentDate, 'MMM', 'en-ZA');

  monthsList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  yearsList = [this.currentYear, this.currentYear - 1, this.currentYear - 2, this.currentYear - 3, this.currentYear - 4];

  openMonthFilter = false;
  openYearFilter = false;
  openFilter = false;

  constructor(private internaluserService: InternaluserService, private conversationService: ConversationService) { }

  ngOnInit(): void {
    //TODO: Fetch user's conversations here
    this.conversationService.getConversationsByUsername(this.bankerObject.username).subscribe((data: Conversation[]) => {
      this.bankerConversations =data.sort(this.compareDates);
      this.bankerConversations = this.bankerConversations.filter(convo => convo.conversationStatus.toLowerCase() === 'new' || convo.conversationStatus.toLowerCase() === 'active');
      this.allBankerConversations = this.bankerConversations;
    });
  }

  getClientName(conversationObj: Conversation){
    return `${conversationObj.customerFirstname} ${conversationObj.custometLastname}`;
  }

  getLastMessageTime(currentConvoObj: Conversation){
    const lastMessage = currentConvoObj.messages[currentConvoObj.messages.length - 1];
    const messageTime = lastMessage !== undefined ?
                         lastMessage.messageTimestamp :
                          new Date().toDateString();

    const formatedTime =  formatDate(messageTime, 'EEEE h:mm a', 'en-ZA');
    return formatedTime;
  }

  getConversationLastMessage(conversation): Message {
    return conversation.messages[conversation.messages.length - 1];
  }

  showAllocateScreen(conversationId){
    this.showConversationAllocateScreen.emit(conversationId);
  }

  conversationHasMessages(conversation){
    return !isNullOrUndefined(conversation.messages) && conversation.messages.length > 0;
  }

  compareDates(convoA: Conversation, convoB: Conversation) {  
    let comparison = 0;
    if(!isNullOrUndefined(convoA.messages) && convoA.messages.length > 0 && !isNullOrUndefined(convoB.messages) && convoB.messages.length > 0){

      const conversationDateA = new Date(convoA.messages[convoA.messages.length - 1].messageTimestamp);
      const conversationDateB = new Date(convoB.messages[convoB.messages.length - 1].messageTimestamp);

      if (conversationDateA.getTime() > conversationDateB.getTime()) {
        comparison = -1;
      } else if (conversationDateA.getTime() < conversationDateB.getTime()) {
        comparison = 1;
      }
    }

    return comparison;
  }

  conversationActive(conversation: Conversation){
    return conversation.conversationStatus.toLowerCase() === 'active';
  }

  toggleFilter(filterType: string){
    if(filterType.toLowerCase() === 'year'){
      this.openYearFilter = !this.openYearFilter;
    }
    else if(filterType.toLowerCase() === 'month'){
      this.openMonthFilter = !this.openMonthFilter;
    }
  }

  setCurrent(filterType: string, newValue){
    if(filterType.toLowerCase() === 'year'){
      this.currentYear = newValue;
    }
    else if(filterType.toLowerCase() === 'month'){
      this.currentMonth = newValue;
    }
    this.filterByDate();
  }

  filterByDate(){

    this.bankerConversations =  this.allBankerConversations.filter(this.compareConversationDates);
  }

  compareConversationDates = (conversation: Conversation) => {
    if(this.conversationHasMessages(conversation)){
      const preferredDate = new Date(this.currentYear + ' ' + this.currentMonth);
      const convoDate = new Date(conversation.messages[0].messageTimestamp);

      if(preferredDate.getFullYear() === convoDate.getFullYear()){
        if(preferredDate.getMonth() === convoDate.getMonth()){
          return true;
        }
      }
      else{
        return false;
      }
    }
    else{
      return false;
    }
  }

  filterChats(filterType: string){
    if(filterType === 'all'){
      this.bankerConversations =  this.allBankerConversations;
    }
    else{
      this.bankerConversations =  this.allBankerConversations.filter(conversation => conversation.conversationStatus.toLowerCase() === filterType);
    }
    this.openFilter = false;
  }

  hasNoConversations(bankerConvos: Conversation[]){
    return bankerConvos.length === 0;
  }

  changeTBStatus(slideChange: MatSlideToggleChange){
    if(slideChange.checked){
      this.internaluserService.deactivateTransactionalBanker(this.bankerObject.username);
    }
  }
}
