import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from "@angular/core";

import { ConversationService } from 'src/app/services/conversation/conversation.service';
import { Conversation } from 'src/app/models/conversation';
import { Customer } from 'src/app/models/customer';
//import { element } from 'protractor';
import { PortfolioService } from 'src/app/services/portfolio/portfolio.service';
import { Portfolio } from 'src/app/models/portfolio';
import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { formatDate } from '@angular/common';
import { InternalUser } from 'src/app/models/internal-user';
import { InternaluserService } from 'src/app/services/internaluser/internaluser.service';
import { environment } from 'src/environments/environment';
import { Message } from 'src/app/models/message';
@Component({
  selector: "app-conversations-bar",
  templateUrl: "./conversations-bar.component.html",
  styleUrls: ["./conversations-bar.component.scss"],
})
export class ConversationsBarComponent implements OnInit {


  @Output() activeItemChange = new EventEmitter();
  @Output() showTransactionalBankers = new EventEmitter();

  @Input() currentTab: string;
  @Input() isTeamLead: boolean;
  @Input() reloadVariable: boolean;
  currentUser = new InternalUser();

  activeConversations: Array<Conversation>;
  passiveConversations: Array<Conversation>;
  storedConversationsForFilter: Array<Conversation>;

  openFilter: boolean = false;
  currentClient: Customer;
  bankerPortfolios: Array<Portfolio>;
  allBankerPortfolios: Array<Portfolio>;
  filteredConversations: Array<Conversation>;
  teamLeads: Array<InternalUser>;

  search: string = "";
  showSearchResults: boolean = false;
  currentItemId = '';
  isPrestigeBanker = false;
  currentUserType = '';
  viewBankers = true;

  constructor(private conversationService: ConversationService, private portfolioService: PortfolioService, private userService: InternaluserService) { }

  ngOnInit() {
    const username = sessionStorage.getItem('msal.username');
    this.currentItemId = '';

    this.userService.getTransactionalBanker(username).subscribe((currentBanker: InternalUser) => {
      this.currentUser = currentBanker;
      const currentUsername = this.currentUser.username;

      this.isPrestigeBanker = this.currentUser.team.teamPrestige;
      this.currentUserType = this.currentUser.userType.userTypeName.toLowerCase();

      if (this.currentTab.toLowerCase() === 'chat' || this.currentTab.toLowerCase() === 'escalations' || this.currentTab.toLowerCase() === '') {
        this.conversationService.retrieveActiveCustomerConversations(currentUsername).subscribe((data: Conversation[]) => {
          data = data.sort(this.compareDates);
          this.passiveConversations = data;
          this.activeConversations = data;
        });
      }
      else if (this.currentTab.toLowerCase() === 'portfolios') {
        this.portfolioService.getPortfoliosByBankerId(currentUsername).subscribe((data: Portfolio[]) => {
          this.allBankerPortfolios = data;
          this.bankerPortfolios = data;
          if(this.currentUser.userType.userTypeName.toLowerCase() === 'team_lead'){
            this.viewAllTransactionalBankers(currentBanker);

            this.userService.getAllTeamLeads().subscribe((data: InternalUser[]) => {
              var result1 = [], result2 = [];
                for (var i = 0; i < data.length; i++) {
                  if (data[i].username.toLowerCase() === this.currentUser.username.toLowerCase()) {
                    result1.push(data[i]);
                  } else {
                    result2.push(data[i]);
                  }
                }
                this.teamLeads = result1.concat(result2);
            });
          }
        });
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.currentItemId = '';
    if ((!isNullOrUndefined(changes.currentTab) && !changes.currentTab.isFirstChange()) || (!isNullOrUndefined(changes.reloadVariable) && changes.reloadVariable.currentValue === true)) {
      const username = sessionStorage.getItem('msal.username');

      this.userService.getTransactionalBanker(username).subscribe((currentBanker: InternalUser) => {
        this.currentUser = currentBanker;
        const currentUsername = this.currentUser.username;
        if (!isNullOrUndefined(changes.currentTab) && (changes.currentTab.currentValue.toLowerCase() === 'chat' || changes.currentTab.currentValue.toLowerCase() === 'escalations') || ( !isNullOrUndefined(changes.reloadVariable) && changes.reloadVariable.currentValue && (this.currentTab.toLocaleLowerCase() === 'chat' || this.currentTab.toLocaleLowerCase() === 'escalations'))) {
          this.conversationService.retrieveActiveCustomerConversations(currentUsername).subscribe((data: Conversation[]) => {
            data = data.sort(this.compareDates);

            this.passiveConversations = data;
            this.activeConversations = data;
          });
        }
        else if (!isNullOrUndefined(changes.currentTab) && changes.currentTab.currentValue.toLowerCase()  === 'portfolios') {
          this.portfolioService.getPortfoliosByBankerId(currentUsername).subscribe((data: Portfolio[]) => {
            this.allBankerPortfolios = data;
            this.bankerPortfolios = data;
            if(this.currentUser.userType.userTypeName.toLowerCase() === 'team_lead'){
              this.viewAllTransactionalBankers(currentBanker);

              this.userService.getAllTeamLeads().subscribe((data: InternalUser[]) => {

                var result1 = [], result2 = [];
                for (var i = 0; i < data.length; i++) {
                  if (data[i].username.toLowerCase() === this.currentUser.username.toLowerCase()) {
                    result1.push(data[i]);
                  } else {
                    result2.push(data[i]);
                  }
                }
                this.teamLeads = result1.concat(result2);
              });
            }
          });
        }
      });
    }
  }

  itemChanged(selectedObj) {
    this.activeItemChange.emit(selectedObj);

    if (!isNullOrUndefined(selectedObj.conversationId)) {
      this.currentItemId = selectedObj.conversationId;
    }
    else if (!isNullOrUndefined(selectedObj.portfolioId)) {
      this.currentItemId = selectedObj.portfolioId;
    }

    this.viewBankers = false;
  }

  getClientName(conversationObj: Conversation) {
      return  `${conversationObj.customerFirstname} ${conversationObj.custometLastname}`;
  }

  getLastMessageTime(currentConvoObj: Conversation) {
    const lastMessage = currentConvoObj.messages[0];
    const messageTime = lastMessage !== undefined ?
      lastMessage.messageTimestamp :
      new Date().toISOString();

    return messageTime;
  }

  getClientObject(conversationObj: Conversation) {
    return {
      convoId: conversationObj.conversationId,
      customerId: conversationObj.customerId,
    };
  }

  viewAllTransactionalBankers(internalUser) {
    this.showTransactionalBankers.emit(internalUser);
    this.viewBankers = true;
    this.currentItemId = '';
    this.openFilter = false;

    this.portfolioService.getPortfoliosByBankerId(internalUser.username).subscribe((data: Portfolio[]) => {
      this.allBankerPortfolios = data;
      this.bankerPortfolios = data;
    });
  }

  conversationHasMessages(conversation) {
    return !isNullOrUndefined(conversation.messages) && conversation.messages.length > 0;
  }

  onSearch(event: any) {
    this.search = event.target.value;
    
    if(this.passiveConversations !== undefined){
      this.filteredConversations = this.passiveConversations.filter((item: Conversation) => {
        item.messages.forEach(element => {
          return element.senderId === this.search;
        });
      });
      if (this.filteredConversations.length === 0 && this.search !== undefined  && this.search !== "") {
        let regex = new RegExp(/^[0-9]*$/);
        let isNum: boolean = regex.test(this.search);
        if (isNum) {
          this.numberSearch();
        } else {
           this.nameSearch();
        }
      }
    }
    
    
    if (this.search === "") {
      this.showSearchResults = false;
    } else this.showSearchResults = true;

    if(this.currentTab.toLowerCase() === 'portfolios'){
      this.portfolioSearch();
    }
  }

  numberSearch = () => {
    //search for ID number and Cell number
    this.passiveConversations.forEach((element) => {
      let idNum = element.customerSAId;
      let cellNum = element.customerMobileNumber;
      const search = this.search.toLowerCase();
      let searchResult = "";
      let searchResultCell = "";

      const shortestIdSearch = search.length < idNum.length ? search.length : idNum.length;
      const shortestCellSearch = search.length < cellNum.length ? search.length : cellNum.length;
      for (let i = 0; i < shortestIdSearch; i++) {
        const character = idNum[i].toLowerCase();
        if (character === search[i]) {
          searchResult += search[i];
          const lengthOfMatchedSearch = searchResult.length;
          const searchElement = idNum.slice(0, lengthOfMatchedSearch);
          if (lengthOfMatchedSearch === search.length) {
            if (searchResult === searchElement) {
              this.filteredConversations.push(element);
            }
          }
        }
      }
      for (let index = 0; index < shortestCellSearch; index++) {
        const character = cellNum[index].toLowerCase();

        if (character === search[index]) {
          searchResultCell += search[index];
          const lengthOfMatchedSearch = searchResultCell.length;
          const searchElement = cellNum.slice(0, lengthOfMatchedSearch);
          if (lengthOfMatchedSearch === search.length) {

            if (searchResultCell === searchElement) {
              this.filteredConversations.push(element);
            }
          }
        }
      }
    });
  };

  nameSearch = () => {
    //search by name
    this.passiveConversations.forEach((element) => {
      let name = this.getClientName(element);
      let name2 = element.custometLastname.toLowerCase();

      name = name.toLowerCase();
      let convStatus = element.conversationStatus.toLowerCase();
      const search = this.search.toLowerCase();
      let searchResult = "";
      let searchResult2 = "";

      const shortest = search.length < name.length ? search.length : name.length;
      for (let index = 0; index < shortest; index++) {
        const character = name[index].toLowerCase();
        if (character == search[index]) {
          searchResult += search[index];
          const lengthOfMatchedSearch = searchResult.length;
          const searchElement = name.slice(0, lengthOfMatchedSearch);
          if (lengthOfMatchedSearch === search.length) {
            if (searchResult === searchElement) {
              this.filteredConversations.push(element);
            }
          }
        }
      }

      for (let index = 0; index < shortest; index++) {
        const character = name2[index].toLowerCase();
        if (character == search[index]) {
          searchResult2 += search[index];
          const lengthOfMatchedSearch = searchResult2.length;
          const searchElement = name2.slice(0, lengthOfMatchedSearch);
          if (lengthOfMatchedSearch === search.length) {
            if (searchResult2 === searchElement) {
              this.filteredConversations.push(element);
            }
          }
        }
      }

      if (name.toLowerCase() === search || element.conversationId.toString() === this.search ||
        search === convStatus) {
        this.filteredConversations.push(element);
      }
    });
  };

  portfolioSearch = () => {
    //search by name
    this.showSearchResults = false;
    this.bankerPortfolios = new Array<Portfolio>();
    this.allBankerPortfolios.forEach((element) => {
      let name = this.getRMName(element);
      name = name.toLowerCase();
      const search = this.search.toLowerCase();
      let searchResult = "";
      const shortest = search.length < name.length ? search.length : name.length;
      for (let index = 0; index < shortest; index++) {
        const character = name[index].toLowerCase();
        if (character == search[index]) {
          searchResult += search[index];
          const lengthOfMatchedSearch = searchResult.length;
          const searchElement = name.slice(0, lengthOfMatchedSearch);
          if (lengthOfMatchedSearch === search.length) {
            if (searchResult === searchElement) {
              this.bankerPortfolios.push(element);
            }
          }
        }
      }
    });

    if(this.search === ""){
      this.bankerPortfolios = this.allBankerPortfolios;
    }
  };

  removeFilters = () => {
    this.openFilter = false;
    this.showSearchResults = true;
    this.filteredConversations = [...this.passiveConversations];
  };

  displayPending = () => {
    this.openFilter = false;
    this.showSearchResults = true;
    this.filteredConversations = [...this.passiveConversations];
    this.filteredConversations = this.filteredConversations.filter(
      (element) => element.conversationStatus === "NEW"
    );
  };

  displayInProgress = () => {
    this.openFilter = false;
    this.showSearchResults = true;
    this.filteredConversations = [...this.passiveConversations];
    this.filteredConversations = this.filteredConversations.filter(
      (element) => element.conversationStatus === "ACTIVE"
    );
  };

  compareDates(convoA: Conversation, convoB: Conversation) {
    let comparison = 0;
    if (!isNullOrUndefined(convoA.messages) && convoA.messages.length > 0 && !isNullOrUndefined(convoB.messages) && convoB.messages.length > 0) {

      const conversationDateA = new Date(convoA.messages[convoA.messages.length - 1].messageTimestamp);
      const conversationDateB = new Date(convoB.messages[convoB.messages.length - 1].messageTimestamp);

      if (conversationDateA.getTime() > conversationDateB.getTime()) {
        comparison = -1;
      } else if (conversationDateA.getTime() < conversationDateB.getTime()) {
        comparison = 1;
      }
    }

    return comparison;
  }

  hasResults(){
    if(this.filteredConversations !== undefined){
      const foundConversations = this.filteredConversations.filter(conv => conv.messages.length > 0);

      return (foundConversations.length > 0);
    }
    else{
      return false;
    }
    
  }

  countUnreadMessages(customerConversation: Conversation){
    const messagesArr = customerConversation.messages;
    const customerId = customerConversation.customerId.toString();

    return messagesArr.filter(msg => msg.senderId === customerId.toLowerCase() && msg.messageStatus.toLowerCase() === 'unread').length;
  }

  hasConversations(convos: Conversation[]){
    return convos !== undefined && (convos.length > 0);
  }

  getSearchText(){
    return 'Search';
    // if(this.currentTab.toLowerCase() === 'portfolios'){
    //   return 'Search by relationship manager name';
    // }
    // else{
    //   return 'Search by client name';
    // }
  }

  getRMName(portfolioObj: Portfolio){
    if(portfolioObj.name !== undefined){
      return `${portfolioObj.name} ${portfolioObj.surname}`;
    }
    else{
      return `${portfolioObj.rmInternalUser.name} ${portfolioObj.rmInternalUser.surname}`;
    }
  }

  getInternalUserNames(userObj: InternalUser){
    if(userObj.username === this.currentUser.username){
      return `My Team`;
    }else{
      return `${userObj.name} ${userObj.surname}`;
    }
  }
}
