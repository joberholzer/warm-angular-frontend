import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  SimpleChanges,
  ViewChild,
  ElementRef
} from "@angular/core";
import { FormControl } from "@angular/forms";
import { ConversationService } from "src/app/services/conversation/conversation.service";
import { MessageService } from "src/app/services/message/message.service";
import { Conversation } from "src/app/models/conversation";
import { Message } from "src/app/models/message";
import { MessageRequest } from 'src/app/models/message-request';
import { isNullOrUndefined } from 'util';
import { formatDate } from '@angular/common';
import { Portfolio } from 'src/app/models/portfolio';


@Component({
  selector: "app-chat-window",
  templateUrl: "./chat-window.component.html",
  styleUrls: ["./chat-window.component.scss"]
})
export class ChatWindowComponent implements OnInit {
  openMenu: boolean = false;

  @Input() currentConversation: Conversation;
  @Input() currentChatId: string;
  @Input() currentCustomerId: string;
  @Input() prevConversation: Conversation;
  @Input() isOldConversation: boolean;
  @Input() canEscalate: boolean;

  currentUserName: string;

  @Output() showClientProfile = new EventEmitter();
  @Output() showModal = new EventEmitter();

  allConvos: Array<Conversation>;
  newMessage = new FormControl("");
  container: HTMLElement;
  conversationMessages: any;
  newMsg: Message;

  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  ngAfterViewChecked() {        
      this.scrollToBottom();        
  } 

  scrollToBottom(): void {
      try {
          this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
      } catch(err) { }                 
  }

  constructor(
    private conversationService: ConversationService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
      if (!this.isConversationHistory()) {
        this.conversationService
          .getConversationMessages(this.currentCustomerId)
          .subscribe((data: Conversation[]) => {
            this.allConvos = data.sort(this.compareDates);

            for(var convoItem of this.allConvos){
              if(this.conversationHasMessages(convoItem)){
                convoItem.messages = convoItem.messages.sort(this.compareMessageDates);
              }
            }
          });
      }
      else {
        this.allConvos = new Array<Conversation>();
        this.allConvos.push(this.prevConversation);
        this.currentConversation = this.prevConversation;
      }
      this.scrollToBottom();
  }

  ngOnChanges(changes: SimpleChanges) {
    if(!changes.currentCustomerId.firstChange){
      this.conversationService
      .getConversationMessages(changes.currentCustomerId.currentValue)
      .subscribe((data: Conversation[]) => {
        this.allConvos = data.sort(this.compareDates);

        for(var convoItem of this.allConvos){
          if(this.conversationHasMessages(convoItem)){
            convoItem.messages = convoItem.messages.sort(this.compareMessageDates);
          }
        }
      });
    }
  }

  onSubmitText() {

    this.newMsg = {
      messageId: "f",
      senderId: this.currentConversation.internalUserUsername,
      messageText: this.newMessage.value,
      messageStatus: "read",
      messageTimestamp: new Date().toUTCString(),
      conversation: null
    };

    const msgReq = new MessageRequest(
      this.currentChatId,
      this.currentConversation.internalUserUsername,
      this.newMessage.value,
      new Date()
    );

    if(isNullOrUndefined(this.allConvos)){
      const newConvo = new Conversation();
      newConvo.messages = new Array<Message>();
      this.allConvos = new Array<Conversation>();
      this.allConvos.push(newConvo);
    }
    this.allConvos[this.allConvos.length - 1].messages.push(this.newMsg);

    this.messageService
      .postWhatsAppMessage(msgReq)
      .subscribe((data: any) => {
        console.log("Message sent ", data);
      });

    this.newMessage.reset();

    this.ngAfterViewInit();
  }

  ngAfterViewInit() {
    this.container = document.getElementById("chatContainer");
    this.container.scrollTop = this.container.scrollHeight + 105;
  }

  displayUserProfile(currentCustomerId) {
    this.showClientProfile.emit(currentCustomerId);
  }

  displayDialog(eventType) {
    const conversationId = this.currentChatId;
    this.showModal.emit({ eventType, conversationId });
  }

  getClientName(): string {
    return `${this.currentConversation.customerFirstname} ${this.currentConversation.custometLastname}`;
  }

  isSentMessage(senderId): boolean {
    return (senderId.toString() !== this.currentCustomerId.toString() || senderId.toString() === 'automated');
  }

  isConversationHistory(): boolean {
    return !isNullOrUndefined(this.isOldConversation) && this.isOldConversation;
  }

  compareDates(convoA: Conversation, convoB: Conversation) {
    let comparison = 0;
    if (!isNullOrUndefined(convoA.messages) && convoA.messages.length > 0 && !isNullOrUndefined(convoB.messages) && convoB.messages.length > 0) {

      const conversationDateA = new Date(convoA.messages[convoA.messages.length - 1].messageTimestamp);
      const conversationDateB = new Date(convoB.messages[convoB.messages.length - 1].messageTimestamp);

      if (conversationDateA.getTime() > conversationDateB.getTime()) {
        comparison = 1;
      } else if (conversationDateA.getTime() < conversationDateB.getTime()) {
        comparison = -1;
      }
    }

    return comparison;
  }

  compareMessageDates(msgA: Message, msgB: Message) {
    let comparison = 0;

    const conversationDateA = new Date(msgA.messageTimestamp);
    const conversationDateB = new Date(msgB.messageTimestamp);

    if (conversationDateA.getTime() > conversationDateB.getTime()) {
      comparison = 1;
    } else if (conversationDateA.getTime() < conversationDateB.getTime()) {
      comparison = -1;
    }

    return comparison;
  }

  conversationHasMessages(conversation) {
    return !isNullOrUndefined(conversation.messages) && conversation.messages.length > 0;
  }

  getRMName(clientPortfolio: Portfolio){
    if(!isNullOrUndefined(clientPortfolio)){
      return `${clientPortfolio.name} ${clientPortfolio.surname}`;
    }
    else{
      return '';
    }
  }

  getBankername(msgObj: Message){
    if(msgObj.senderId.toLowerCase() === 'automated'){
      return 'automated';
    }
    else{
      return `${this.currentConversation.dtoPortfolio.name} ${this.currentConversation.dtoPortfolio.surname}`;
    }
  }
}
