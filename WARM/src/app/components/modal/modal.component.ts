import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ConversationService } from "src/app/services/conversation/conversation.service";
import { AuthService } from 'src/app/services/authService/auth.service';

export interface DialogData {
  modalTitle: string;
  modalMessage: string;
  modalPrimaryButton: string;
  modalSecondaryButton: string;
  conversationId?: number;
}
@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.scss"]
})
export class ModalComponent {
  constructor(
    public dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private conversationService: ConversationService,
    private authService: AuthService
  ) {}

  onPrimaryButtonClick(data): void {
    if (data.type.toLowerCase() === "close") {
      this.conversationService
        .closeActiveCustomerConversation(data.conversationId)
        .subscribe(response => {
          if (response.conversationStatus === "CLOSED") {
            console.log("conversation successfully closed");
          }
          console.log(response);
        });
    }

    if (data.type.toLowerCase() === "escalate") {
      this.conversationService.escalateConversation(data.conversationId).subscribe(response => {
        console.log(response);
      });
    }
    else if(data.type.toLowerCase() === "reallocate") {
      const updateConv = {
        reallocateType: data.type,
        conversationId: data.conversationId,
        username: data.username
      };
      this.conversationService.reAllocateReferConversation(updateConv).subscribe(res =>{
          console.log('response', res);
      });
    }
    else if(data.type.toLowerCase() === "logout") {
      this.authService.signOut();
    }
    this.dialogRef.close(data.type.toLowerCase());
  }
}
