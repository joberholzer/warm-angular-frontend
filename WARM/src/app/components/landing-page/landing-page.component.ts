import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { environment } from 'src/environments/environment';
import { InternalUser } from 'src/app/models/internal-user';
import { InternaluserService } from 'src/app/services/internaluser/internaluser.service';


@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  username: string = '';
  @Input() currentUser: InternalUser;
  
  constructor(private userService: InternaluserService) {}

  ngOnInit(): void {
    this.username = this.currentUser.name + ' ' + this.currentUser.surname;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.username = changes.currentUser.currentValue.name + ' ' + changes.currentUser.currentValue.surname;
  }

  hasLoadedUser(){
    return this.currentUser.name !== undefined;
  }
}
