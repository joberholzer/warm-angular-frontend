import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class LoggingService {
  logError(message: string, stack: string) {
    // TOTDO: log the errors in the DB
    console.log("LoggingService: " + message);
  }
}
