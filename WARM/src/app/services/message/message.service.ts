import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Message } from 'src/app/models/message';
import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MessageRequest } from 'src/app/models/message-request';
import HttpHelper from 'src/app/helpers/httpHelper';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class MessageService {

  reqBody: any;

  constructor(private http: HttpHelper) { }

  postWhatsAppMessage(newMessage: MessageRequest): Observable<Message> {
      return this.http.post<Message>(`${environment.apiUrl}/api/post/send_message`, newMessage);
  }
}
