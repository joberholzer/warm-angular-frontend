import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { InternalUser } from 'src/app/models/internal-user';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import HttpHelper from 'src/app/helpers/httpHelper';
import { Team } from 'src/app/models/team';
import { Cst } from 'src/app/models/cst';
import { UserType } from 'src/app/models/user-type';
import { catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class InternaluserService {
  constructor(private http: HttpHelper, private router: Router) {}
  // getTransactionalBanker(username: string): Observable<InternalUser> {
  //   if(username === 'TEAM_LEAD'){
  //     return this.http.get<InternalUser>(`${environment.apiUrl}/api/get/internaluser/TeamLead 1`);
  //   }
  //   else if(username === 'RELATIONSHIP_MANAGER'){
  //     return this.http.get<InternalUser>(`${environment.apiUrl}/api/get/internaluser/RelationshipManager 2`);
  //   }
  //   else if(username === 'TRANSACTIONAL_BANKER'){
  //     return this.http.get<InternalUser>(`${environment.apiUrl}/api/get/internaluser/TransactionalBanker 1`);
  //   }
  //   else if(username === 'PRESTIGE_BANKER'){
  //     return this.http.get<InternalUser>(`${environment.apiUrl}/api/get/internaluser/Prestige Banker 2`);
  //   }
  //   else if(username === 'PRESTIGE_LEAD'){
  //     return this.http.get<InternalUser>(`${environment.apiUrl}/api/get/internaluser/Team Lead PB`);
  //   }
  //   else{
  //     return this.http.get<InternalUser>(`${environment.apiUrl}/api/get/internaluser/TransactionalBanker 2`);
  //   }
  // }


  getTransactionalBanker(username: string): Observable<InternalUser> {
    return this.http.get<InternalUser>(`${environment.apiUrl}/api/get/internaluser/${username}`).pipe(
      tap( // Log the result or error
        data => {
          if(data === null){
            console.log('User could not be authorized.Redirecting');
            this.router.navigate(['unauthorized']);
          }
        },
        error => {
          console.log('User unauthorized.Redirecting', error);
          this.router.navigate(['unauthorized']);
        }
      )
    );
  }
  getTransactionalBankersPerTeam(teamId: string): Observable<Array<InternalUser>> {
    return this.http.get<Array<InternalUser>>(`${environment.apiUrl}/api/get/transactionalbankers/${teamId}`);
  }

  deactivateTransactionalBanker(username: string): Observable<InternalUser> {
    return this.http.put<InternalUser>(`${environment.apiUrl}api/update/user/deactivatetb/${username}`, username);
  }

  getAllTeamLeads(): Observable<Array<InternalUser>> {
    return this.http.get<Array<InternalUser>>(`${environment.apiUrl}/api/get/teamleads`);
  }
}
