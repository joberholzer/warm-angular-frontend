import { TestBed } from '@angular/core/testing';

import { InternaluserService } from './internaluser.service';

describe('InternaluserService', () => {
  let service: InternaluserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InternaluserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
