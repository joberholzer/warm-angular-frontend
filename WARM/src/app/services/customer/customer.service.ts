import { Injectable } from '@angular/core';
import { Customer } from 'src/app/models/customer';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import HttpHelper from 'src/app/helpers/httpHelper';
@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor(private http: HttpHelper) { }

  getCustomerById(customerId: string): Observable<Customer> {
    return this.http.get<Customer>(`${environment.apiUrl}/api/get/customer/${customerId}`);
  }

  getCustomersByPortfolioId(portfolioId: string): Observable<Array<Customer>> {
    return this.http.get<Array<Customer>>(`${environment.apiUrl}/api/get/customers/${portfolioId}`);
  }
}