import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Portfolio } from 'src/app/models/portfolio';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import HttpHelper from 'src/app/helpers/httpHelper';

@Injectable({
  providedIn: 'root'
})
export class PortfolioService {
  constructor(private http: HttpHelper) { }

  getPortfoliosByBankerId(username: string): Observable<Array<Portfolio>> {
    return this.http.get<Array<Portfolio>>(`${environment.apiUrl}/api/get/portfolios/${username}`);
  }

  getAllPortfolios(): Observable<Array<Portfolio>> {
    return this.http.get<Array<Portfolio>>(`${environment.apiUrl}/api/get/portfolios`);
  }
}
