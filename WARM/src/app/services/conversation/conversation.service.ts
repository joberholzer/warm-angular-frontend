import { Injectable } from "@angular/core";
import { Conversation } from "src/app/models/conversation";
import { Observable } from "rxjs";
import { environment } from 'src/environments/environment';
import HttpHelper from 'src/app/helpers/httpHelper';

@Injectable({
  providedIn: "root"
})
export class ConversationService {
  constructor(private http: HttpHelper) {}
  getConversationsByCustomerId(customerId: string): Observable<Array<Conversation>> {
    return this.http.get<Array<Conversation>>(`${environment.apiUrl}/api/get/conversations/history/${customerId}`);
  }

  getConversationsByUsername(username: string): Observable<Array<Conversation>> {
    return this.http.get<Array<Conversation>>(`${environment.apiUrl}/api/get/conversations/internaluser/${username}`);
  }

  getConversationMessages(customerId: string): Observable<Array<Conversation>> {
    return this.http.get<Array<Conversation>>(`${environment.apiUrl}/api/get/conversation/messages/${customerId}`);
  }

  getConversationById(conversationId: string): Observable<Conversation> {
    return this.http.get<Conversation>(`${environment.apiUrl}/api/get/conversation/${conversationId}`);
  }

  getConversationsActiveConversationId(conversationId: string): Observable<Conversation> {
    return this.http.get<Conversation>(`${environment.apiUrl}/api/get/conversations/${conversationId}`);
  }

  escalateConversation(conversationId: number): Observable<Conversation> {
    return this.http.put<Conversation>(`${environment.apiUrl}/api/update/conversation/escalate/${conversationId}`, conversationId);
  }

  retrieveActiveCustomerConversations(username: string): Observable<Array<Conversation>> {
    return this.http.get<Array<Conversation>>(`${environment.apiUrl}/api/get/conversations/active/${username}`);
  }

  closeActiveCustomerConversation(conversationId: string): Observable<Conversation> {
    return this.http.put<Conversation>(`${environment.apiUrl}/api/update/conversation/close/${conversationId}`,
      conversationId
    );
  }

  reAllocateReferConversation(convoUpdateRequestObject): Observable<Conversation> {
    return this.http.put<Conversation>(
      `${environment.apiUrl}/api/update/conversation`,
      JSON.stringify(convoUpdateRequestObject)
    );
  }

  getEscalatedConversations(): Observable<Array<Conversation>> {
    return this.http.get<Array<Conversation>>(`${environment.apiUrl}/api/get/conversations/escalated`);
  }
}
