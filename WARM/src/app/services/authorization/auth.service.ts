// import { Injectable } from "@angular/core";
// import { HttpClient, HttpHeaders } from "@angular/common/http";
// import { Observable, BehaviorSubject } from "rxjs";
// import { startWith, map, tap, merge, catchError, retry } from "rxjs/operators";
// import { User } from "../../models/user";
// import { environment } from "../../../environments/environment";
// import { JwtResponse } from "../../models/jwt-response";

// interface LoginDetails {
//   username: string;
//   password: string;
// }

// interface LoginResponse {
//   JWT: string;
//   code: string;
// }

// interface UserData {
//   JWT: string;
//   code: string;
// }

// const loginResponseToUserData = (loginResponse: LoginResponse): UserData => ({
//   JWT: loginResponse.JWT,
//   code: loginResponse.code
// });

// @Injectable({
//   providedIn: "root"
// })
// export class AuthService {
//   public currentUser: Observable<User>;
//   private currentUserSubject: BehaviorSubject<User>;

//   constructor(private http: HttpClient) {
//     this.currentUserSubject = new BehaviorSubject<User>(
//       JSON.parse(localStorage.getItem("currentUser"))
//     );
//     this.currentUser = this.currentUserSubject.asObservable();
//     //authSubject  =  new  BehaviorSubject(false);
//   }

//   public get currentUserValue(): User {
//     return this.currentUserSubject.value;
//   }

//   Login(user: User): Observable<JwtResponse> {
//     let expireTime: Date = new Date();
//     expireTime.setHours(expireTime.getHours() + 8);
//     const httpOptions = {
//       headers: new HttpHeaders({
//         "Content-Type": "application/json"
//       })
//     };
//     //this http.post function sends the body as JSON
//     return (
//       this.http
//         .post<LoginResponse>(`${environment.apiUrl}`, user, httpOptions)
//         // .pipe(retry(2), map(loginResponseToUserData));
//         .pipe(
//           tap(async (res: JwtResponse) => {
//             if (res.JWT) {
//               localStorage.setItem("ACCESS_TOKEN", res.JWT);
//               localStorage.setItem("EXPIRES_IN", expireTime.toString());
//               user.token = res.JWT;
//               return user;
//             }
//           })
//         )
//     );
//   }

//   logout() {
//     // remove user from local storage to log user out
//     localStorage.removeItem("currentUser");
//   }
// }
