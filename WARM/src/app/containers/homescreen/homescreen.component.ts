import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ModalComponent } from "src/app/components/modal/modal.component";
import { Conversation } from 'src/app/models/conversation';
import { Customer } from 'src/app/models/customer';
import { ConversationService } from 'src/app/services/conversation/conversation.service';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { Portfolio } from 'src/app/models/portfolio';
import { InternalUser } from 'src/app/models/internal-user';
import { PortfolioService } from 'src/app/services/portfolio/portfolio.service';
import { InternaluserService } from 'src/app/services/internaluser/internaluser.service';
import { environment } from 'src/environments/environment';
import { RouterOutlet, ActivatedRoute } from '@angular/router';
import { type } from 'os';
import { isNullOrUndefined } from 'util';
import { Message } from 'src/app/models/message';
import { timer } from 'rxjs';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: "app-homescreen",
  templateUrl: "./homescreen.component.html",
  styleUrls: ["./homescreen.component.scss"]
})
export class HomescreenComponent implements OnInit {
  currentConversation: Conversation;
  currentPortfolio: Portfolio;
  reAllocateConversation: boolean = false;
  selectedBanker: InternalUser;
  preferredConvo: Conversation;
  currentUser = new InternalUser();
  isPrestigeBanker = false;
  isRelationshipManager = false;
  isTeamLead = false;
  showClientInfo = false;
  isHistoryConversation: boolean;
  reloadConversationsbar= false;
  hasUserData = false;
  activeChat: string;
  currentClientId: string;
  currentConversationId: string;

  currentTab = "";

  currentScreen = "";

  escalateData = {
    modalTitle: "Escalate conversation",
    modalMessage: "Are you sure you want to escalate this conversation?",
    modalPrimaryButton: "Escalate",
    modalSecondaryButton: "CANCEL",
    conversationId: null,
    username: null,
    type: 'escalate'

  };

  referData = {
    modalTitle: "Refer conversation",
    modalMessage: "Are you sure you want to refer this conversation?",
    modalPrimaryButton: "Refer",
    modalSecondaryButton: "CANCEL",
    conversationId: null,
    username: null,
    type: 'refer'

  };

  closeData = {
    modalTitle: "Close query?",
    modalMessage: "Are you sure you want to close the query?",
    modalPrimaryButton: "CLOSE",
    modalSecondaryButton: "CANCEL",
    conversationId: null,
    username: null,
    type: 'close'

  };

  logoutData = {
    modalTitle: "Sign out",
    modalMessage: "Are you sure you want to signout?",
    modalPrimaryButton: "SIGN OUT",
    modalSecondaryButton: "CANCEL",
    conversationId: null,
    username: null,
    type: 'logout'

  };

  allocateData = {
    modalTitle: "Allocate conversation",
    modalMessage: "Are you sure you want to allocate this conversation?",
    modalPrimaryButton: "ALLOCATE",
    modalSecondaryButton: "CANCEL",
    conversationId: null,
    username: null,
    type: 'reallocate'
  };

  twoHoursAlertData = {
    modalTitle: "Close conversations",
    modalMessage: "You have pending conversation(s). You have now reached your 2hrs SLA.",
    modalPrimaryButton: "CLOSE",
    modalSecondaryButton: "CANCEL",
    conversationId: null,
    username: null,
    type: 'reminder_two'
  };

  fourHoursAlertData = {
    modalTitle: "Close conversations",
    modalMessage: "You still have pending conversation(s). Remember to adhere to the sunset principle in order for the conversation(s) to not be escalated to your Team Lead.",
    modalPrimaryButton: "CLOSE",
    modalSecondaryButton: "CANCEL",
    conversationId: null,
    username: null,
    type: 'reminder_four'
  };

  twelveHoursAlertData = {
    modalTitle: "Conversation(s) escalated",
    modalMessage: "You had pending conversation(s) which have since been escalated to your Team Lead.",
    modalPrimaryButton: "CLOSE",
    modalSecondaryButton: "CANCEL",
    conversationId: null,
    username: null,
    type: 'reminder_twelve'
  };

  menuActions = [
    {
      label: "Chat"
    },
    {
      label: "Portfolios"
    },
    {
      label: "Logout"
    }
  ];

  menuActionsTL = [
    {
      label: "Portfolios"
    },
    {
      label: "Escalations"
    },
    {
      label: "Logout"
    }
  ];

  constructor(
    public dialog: MatDialog, 
    private conversationService: ConversationService, 
    private portfolioService: PortfolioService, 
    private userService: InternaluserService, 
    private route: ActivatedRoute) {
      
      let storedToken = sessionStorage.getItem('msal.idtoken');
      let decodedToken = jwt_decode(storedToken);
      let username = decodedToken['onpremisessamaccountname'];

      sessionStorage.setItem('msal.username', username);
    }

  ngOnInit(): void {

    // this.route.data
    // .subscribe(data => {
    //   sessionStorage.setItem('msal.username', data.userType);
    // });

    const username = sessionStorage.getItem('msal.username');

    this.userService.getTransactionalBanker(username).subscribe((currentBanker: InternalUser) => {
      this.currentUser = currentBanker;
      this.isRelationshipManager = currentBanker.userType.userTypeName === 'RELATIONSHIP_MANAGER';
      this.isPrestigeBanker = currentBanker.team.teamPrestige;
      this.isTeamLead = currentBanker.userType.userTypeName === 'TEAM_LEAD' 
                        || currentBanker.userType.userTypeName === 'RELATIONSHIP_MANAGER';

      if(this.isRelationshipManager){
        this.setCurrentTab('portfolios');
      }
      if(this.isTeamLead){
        this.currentTab = 'portfolios';
      }
      else{
        this.currentTab = 'chat';
      }
      const timeInterval = 3600 * 1000;
      timer(timeInterval, timeInterval).subscribe(() =>  this.checkConversationStatus());

      this.hasUserData = true;
    });
  }

  setCurrentTab(currentTab: string){
    this.currentTab = currentTab.toLowerCase();
    this.currentScreen = "";

    if(currentTab.toLowerCase() === 'logout'){
      this.openDialog({eventType: 'logout', conversationId: null});
    }

    if(this.shouldHideConvoBar() && currentTab.toLowerCase() === 'portfolios'){
      let newPortfolio;
      this.portfolioService.getPortfoliosByBankerId(this.currentUser.username).subscribe((portflios: Portfolio[]) => {
        newPortfolio = portflios.pop();
        this.changeActivePortfolio(newPortfolio);
      });
    }
  }

  getActiveChat() {
    return this.activeChat;
  }

  getActiveClient(){
    return  this.currentClientId;
  }

  getCurrentConversation(){
    return this.currentConversation;
  }

  getCurrentPortfolio(){
    return this.currentPortfolio;
  }

  getSelectedBanker(){
    return this.selectedBanker;
  }

  getCurrentUser(){
    return this.currentUser;
  }

  changeActiveChat(currentConvoObject) {
    if(!isNullOrUndefined(currentConvoObject) && (this.currentTab.toLowerCase() === 'chat' || this.currentTab.toLowerCase() === 'escalations')){
      this.isHistoryConversation = false;
      this.currentScreen = "chat";
      this.currentConversation = currentConvoObject;
      this.activeChat = currentConvoObject.conversationId;
      this.currentClientId = currentConvoObject.customerId;
    }
    else{
      this.changeActivePortfolio(this.currentPortfolio);
    }
  }

  changeActivePortfolio(currentPortfolioObject) {
    this.currentScreen = "Relationships";
    this.currentPortfolio = currentPortfolioObject;
  }

  showClientProfile(currentClientId) {
    this.currentClientId = currentClientId;
    this.currentScreen = "Client";
  }

  selectBanker(bankerObj) {

    if(bankerObj.isReAllocation){
      this.openDialog({eventType:'allocate', username:bankerObj.bankerObj.username, conversationId: this.currentConversationId});
    }
    else{
      this.selectedBanker = bankerObj.bankerObj;
      this.currentScreen = "TB Profile";
    }
  }

  showConversation(conversation) {
    this.isHistoryConversation = true;
    this.preferredConvo = conversation;
    this.currentScreen = "chat";
  }

  getPrevConversation(){
    return this.preferredConvo;
  }

  openDialog(convoObject): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: "620px",
      data: this.getDialogData(convoObject)
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog was closed by action", result);

      if(result.toLowerCase() === "close" 
          || result.toLowerCase() === "escalate" 
            || result.toLowerCase() === "reallocate")
      {
        this.currentScreen = '';
        this.reloadConversationsbar = true;
      }
    });
  }

  getDialogData(dialogRequest){
    let dialogData = this.closeData;
    switch (dialogRequest.eventType.toLowerCase()) {
      case 'refer':
        dialogData =  this.referData;
        break;

      case 'allocate':
        dialogData =  this.allocateData;
        break;

      case 'logout':
        dialogData =  this.logoutData;
        break;

      case 'close':
        dialogData = this.closeData;
        break;

      case 'escalate':
        dialogData = this.escalateData;
        break;

      case 'reminder_two':
        dialogData = this.twoHoursAlertData;
        break;

      case 'reminder_four':
        dialogData = this.fourHoursAlertData;
        break;

      case 'reminder_twelve':
        dialogData = this.twelveHoursAlertData;
        break;

      default:
        break;
    }
    dialogData.conversationId = dialogRequest.conversationId;
    dialogData.username = dialogRequest.username;

    return dialogData;
  }

  displayTransactionalBankers(allocationObj, isReAllocation: boolean){
    this.currentScreen = "Team Leads";


    if(allocationObj !== '' && isNullOrUndefined(allocationObj.username)){
      this.currentConversationId = allocationObj;
      this.reAllocateConversation = isReAllocation;
    }
    else{
      this.reAllocateConversation = false;
      this.selectedBanker = allocationObj;
    }
  }

  getCurrentTab(){
    return this.currentTab;
  }

  getActiveConversations(userName){
    return this.conversationService.retrieveActiveCustomerConversations(userName);
  }

  allocateConversation(reallocationObject){
    this.openDialog(reallocationObject);
  }

  shouldHideConvoBar(): boolean{
    return this.currentTab.toLowerCase() === 'portfolios' && (this.isRelationshipManager);
  }

  getScreenState(){
    return this.reAllocateConversation;
  }

  getConversationType(){
    return this.isHistoryConversation;
    
  }

  getUserType(outlet: RouterOutlet) {
    const type = outlet && outlet.activatedRouteData && outlet.activatedRouteData['userType'];
    console.log('User is ', type);
    return type;
  }

  shouldReload(){
    return this.reloadConversationsbar;
  }

  getConversationsNumber(){
    if(this.currentUser.conversations !== undefined){
      const filteredConvos =  this.currentUser.conversations.filter(conversation => (conversation.messages.length > 0 || conversation.conversationParentId !== null) && (conversation.conversationStatus.toLowerCase() === 'new' || conversation.conversationStatus.toLowerCase() === 'active'));
      return filteredConvos.length;
    }
    else{
      return 0;
    }
  }

  checkConversationStatus(){

    this.conversationService.retrieveActiveCustomerConversations(this.currentUser.username).subscribe((activeConvos: Conversation[]) => {
      const filteredConvos = activeConvos.filter(conversation => (conversation.messages.length > 0 || conversation.conversationParentId !== null) && (conversation.conversationStatus.toLowerCase() === 'new'));
      
      let twoHourConvos = false;
      let fourHourConvos = false;
      let twelveHourConvos = false;

      for(const convo of filteredConvos){
        const clientId = convo.customerId.toString();
        const recievedMsgs = convo.messages.filter( msg => msg.senderId.toLowerCase() === clientId.toLowerCase() )
        const sortedMsgs = recievedMsgs.sort(this.compareMessageDates);
        const lastMsgDate = new Date(sortedMsgs[sortedMsgs.length - 1].messageTimestamp);
        const currentDate = new Date();

        let diff = (lastMsgDate.getTime() - currentDate.getTime()) / 1000;
        diff /= (60 * 60);
        const timeDif = Math.abs(Math.round(diff));

        if (timeDif >= 2) {
          twoHourConvos = true;
        } else if (timeDif >= 4) {
          fourHourConvos = true;
        } else if (timeDif >= 12) {
          twelveHourConvos = true;
        }
      }

      if (twoHourConvos) {
        this.openDialog({eventType: 'reminder_two', username: '', conversationId: this.currentConversationId});
      }
      if (fourHourConvos) {
        this.openDialog({eventType: 'reminder_four', username: '', conversationId: this.currentConversationId});
      }
      if (twelveHourConvos) {
        this.openDialog({eventType: 'reminder_twelve', username: '', conversationId: this.currentConversationId});
      }
    });

  }

  compareMessageDates(msgA: Message, msgB: Message) {
    let comparison = 0;

    const conversationDateA = new Date(msgA.messageTimestamp);
    const conversationDateB = new Date(msgB.messageTimestamp);

    if (conversationDateA.getTime() > conversationDateB.getTime()) {
      comparison = 1;
    } else if (conversationDateA.getTime() < conversationDateB.getTime()) {
      comparison = -1;
    }

    return comparison;
  }

  checkIfRelationshipManager(){
    return this.isRelationshipManager;
  }
}
