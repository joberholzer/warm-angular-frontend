import { Component, OnInit } from '@angular/core';
import { ModalComponent } from "src/app/components/modal/modal.component";
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.scss']
})
export class UnauthorizedComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
    this.openDialog();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      disableClose: true,
      width: "620px",
      data: {
        modalTitle: "Unauthorized User",
        modalMessage: "You are not authorized to access this application.",
        modalPrimaryButton: "SIGN OUT",
        modalSecondaryButton: null,
        conversationId: null,
        username: null,
        type: 'logout'
      }
    });
  }
}
