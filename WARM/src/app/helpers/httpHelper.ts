import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
    providedIn: "root"
})
export default class HttpHelper {

    constructor(private http: HttpClient) {}
     
    get<T>(requestURL: string): Observable<T> {
        return this.http.get<T>(requestURL, httpOptions);
    }
    post<T>(requestURL: string, requestBody: any): Observable<T>{
        return this.http.post<T>(requestURL,  JSON.stringify(requestBody), httpOptions);
    }
    put<T>(requestURL: string, requestParameter: any): Observable<T>{ 
        return this.http.put<T>(requestURL, requestParameter, httpOptions);
    }
}