import { Portfolio } from './portfolio';
import { Conversation } from './conversation';

export class Customer {
    customerId: number;
    dtoPortfolio: Portfolio;
    firstName: string;
    middleName: string;
    lastName: string;
    mobileNumber: string;
    conversations: Array<Conversation>;
    customerSAId: string;
    customerEmail: string;
    currentAccountNumber: string;
}
