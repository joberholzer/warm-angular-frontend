import { Conversation } from './conversation';
import { Team } from './team';
import { Cst } from './cst';
import { UserType } from './user-type';

export class InternalUser {
    username: string;
    cst: Cst;
    userType: UserType;
    team: Team;
    name: string;
    surname: string;
    password: string;
    status: boolean;
    dtoConversations: Array<Conversation>;
    conversations: Array<Conversation>;
}
