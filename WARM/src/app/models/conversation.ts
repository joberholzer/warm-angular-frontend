import { Customer } from './customer';
import { InternalUser } from './internal-user';
import { Message } from './message';
import { Portfolio } from './portfolio';

export class Conversation {
    createdAt: string;
    updatedAt: string;
    conversationId: string;
    customerId: string;
    customerFirstname: string;
    custometLastname: string;
    dtoPortfolio: Portfolio;
    internalUserUsername: string;
    conversationStatus: string;
    conversationParentId: string;
    messages: Array<Message>;
    customerSAId: string;
    customerEmail: string;
    customerMobileNumber: string;
}
