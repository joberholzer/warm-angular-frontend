export class User {
  username: string;
  displayName: string;
  password: string;
  token: string;
  code: number;
}
