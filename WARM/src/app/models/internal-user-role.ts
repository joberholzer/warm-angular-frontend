import { InternalUser } from './internal-user';
import { Role } from './role';

export class InternalUserRole {
    userRoleAssignId: number;
    internalUser: InternalUser;
    role: Role;
}
