export class MessageRequest {
    conversationId: string;
    username: string;
    messageText: string;
    timestamp: Date;


    constructor(conversationId: string, username: string, messageText: string, timestamp: Date){
        this.conversationId = conversationId;
        this.username = username;
        this.messageText = messageText;
        this.timestamp = timestamp;
    }
}
