import { Team } from './team';
import { InternalUser } from './internal-user';

export class Portfolio {
    portfolioId: string;
    rmInternalUser: InternalUser;
    username: string;
    name: string;
    surname: string;
    team: Team;
}
