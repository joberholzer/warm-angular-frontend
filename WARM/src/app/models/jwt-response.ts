export interface JwtResponse {
  //   user: {
  //     email: string;
  //     access_token: string;
  //     expires_in: number;
  //   };
  JWT: string;
  code: string;
  expires_in: number;
}
