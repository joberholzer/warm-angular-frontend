import { Conversation } from './conversation';

export class Message {
    messageId: string;
    conversation: Conversation;
    senderId: string;
    messageText: string;
    messageStatus: string;
    messageTimestamp: string;
}
