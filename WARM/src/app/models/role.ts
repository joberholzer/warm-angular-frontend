import { InternalUserRole } from './internal-user-role';

export class Role {
    roleId: number;
    roleName: string;
    internalUsers: Array<InternalUserRole>;
}
