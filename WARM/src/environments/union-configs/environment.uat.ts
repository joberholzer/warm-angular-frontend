export const environment = {
    dev: true,
    sit: false,
    production: false,
    name: "uat",
    redirectURl:"https://warm-angular-warm-app-uat.union.standardbank.co.za/",
    apiUrl: "https://warm-business-service-warm-app-uat.union.standardbank.co.za"
  };