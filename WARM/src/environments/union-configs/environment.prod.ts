export const environment = {
  dev: false,
  sit: false,
  production: true,
  name: "prod",
  redirectURl:"https://warm-angular-warm-app-prod.union.standardbank.co.za/",
  apiUrl: "https://warm-business-service-warm-app-prod.union.standardbank.co.za"
};
