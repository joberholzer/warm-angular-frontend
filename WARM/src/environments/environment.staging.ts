export const environment = {
    dev: false,
    sit: true,
    production: false,
    name: "staging",
    redirectURl:"https://warm-angular-warm-app-staging.fusion.standardbank.co.za/",
    apiUrl: "https://warm-business-service-warm-app-staging.fusion.standardbank.co.za"
  };